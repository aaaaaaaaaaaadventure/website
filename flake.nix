{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {self, nixpkgs, utils}:
  let out = system:
  let pkgs = nixpkgs.legacyPackages."${system}";
  in rec {
    packages.website = pkgs.stdenv.mkDerivation {
      inherit system;
      name = "a12dventure-website";
      src = self;
      buildInputs = [
        pkgs.php
      ];
      buildPhase = ''
        ./build.sh
      '';
      installPhase = ''
        mv public $out
      '';
    };
    defaultPackage = packages.website;
  }; in with utils.lib; eachSystem defaultSystems out
  // {
    nixosModule = { config, ... }: {
      services.nginx.virtualHosts = {
        "aaaaaaaaaaaadventu.re" = {
          enableACME = true;
          forceSSL = true;
          root = self.packages."${config.nixpkgs.system}".website;
        };
      };
    };
  };
}
