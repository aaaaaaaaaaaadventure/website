#!/bin/sh

mkdir .public
cp *.png .public
php index.html.php >.public/index.html
php replace_slugs.sh.php >.public/replace_slugs.sh
cp replace_slugs2.sh .public/replace_slugs2.sh
php replace_slugs.jq.php >.public/replace_slugs.jq
cp -r departures .public
mv .public public
tar -cJf website.tar.xz public
