<!DOCTYPE html>
<html lang="en">
	<head>
	<meta name="viewport" content= "width=device-width, initial-scale=1.0">
		<meta charset="utf-8">
		<title>AAAAAAAAAAAAdventu.re</title>
		<style>
			html {
				height: 100%;
				width: 100%;
				background-color: black;
			}
			#map {
				display: block;
				width: 704px;
				height: 384px;
				margin-left: auto;
				margin-right: auto;
			}
			#logo {
                display: block;
                margin-left: auto;
                margin-right: auto;
                image-rendering: crisp-edges;
				margin-top: 16px;
				margin-bottom: 16px;
				overflow-y: hidden;
			}
			.card {
				max-width: 720px;
				margin-left: auto;
				margin-right: auto;
				margin-top: 8px;
				margin-bottom: 8px;
				padding: 8px;
				background-color: white;
				border-radius: 8px;
				overflow-x: auto;
				overflow-y: hidden;
			}
			table, th, td {
				border: 1px solid white;
				border-collapse: collapse;
				background-color: whitesmoke;
				padding: 8px;
			}
			.pagetitle {
				margin: 0;
				font-family: sans-serif;
				font-size: 2em;
				text-align: center;
			}
			@media only screen and (min-width: 1408px) {
				#map {
					width: 1408px;
					height: 768px;
				}
				#logo {
                    padding-top: 23px;
                    padding-bottom: 23px;
                    transform: scale(3,3);
				}
				.card {
					max-width: 1424px;
				}
				.pagetitle {
					font-size: 4em;
				}
			}
			svg image {
				image-rendering: crisp-edges;
			}
			svg .map-selection {
				display: none;
			}
			svg .map-icon:hover + .map-selection, svg .map-selection:hover {
				display: block;
			}
		</style>
	</head>
	<body>
		<img id="logo" src="logo.png">
		<div class="card">
			<svg id="map" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 22 12" version="1.1">
			<image xlink:href="austria.png" y="0" x="0" height="12" width="22" />
<?php
	$spacesfile = fopen("maps.csv","r");
	while (($space = fgetcsv($spacesfile,0,"\t")) !== false) {
?>				<a xlink:show="new" xlink:href="https://play.aaaaaaaaaaaadventu.re/_/global/<?php echo preg_replace("/https:\/\//","",$space[2]) ?>/main.json">
					<image class="map-icon" xlink:href="<?php echo $space[0] ?>.png" y="<?php echo $space[4] ?>" x="<?php echo $space[5] ?>" height="1" width="1" />
					<image class="map-selection" xlink:href="<?php echo $space[0] ?>-sel.png" y="<?php echo $space[4]?>" x="<?php if($space[6]<0){ echo $space[5]+$space[6]; }else{ echo $space[5]; }?>" height="1" width="<?php echo abs($space[6])+1 ?>" />
				</a>
<?php
	} 
?>			</svg>
		</div>
		<div class="card">
			<table>
				<tr>
					<th>Name</th>
					<th>Map</th>
					<th>Repository</th>
				</tr>
<?php
	fseek($spacesfile, 0);
	while (($space = fgetcsv($spacesfile,0,"\t")) !== false) {
?>				<tr>
					<td><?php echo $space[1] ?></td>
					<td><a href="https://play.aaaaaaaaaaaadventu.re/_/global/<?php echo preg_replace("/https:\/\//","",$space[2]) ?>/main.json"><?php echo preg_replace("/https:\/\//","",$space[2]) ?>/main.json</a></td>
					<td><a href="<?php echo $space[3] ?>"><?php echo preg_replace("/https:\/\//","",$space[3]) ?></a></td>
				</tr>
<?php
	}
?>			</table>
		</div>
		<div class="card">
			AAAAAAAAAAAAdventu.re is part of the <a href="https://fediventure.net/">fediventure</a> project. We run <a href="https://github.com/workadventure-xce/workadventure-xce">workadventure-xce</a>.
		</div>
		<div class="card">
			<ul>
				<li><a href="https://matrix.to/#/#Aaaaaaaaaaaadventure:fairydust.space">#Aaaaaaaaaaaadventure:fairydust.space Matrix Room</a></li>
				<li><a href="replace_slugs.jq">JQ Script for CI to replace rC3 2021-Style slugs. (world://slug/whatever)</a></li>
			</ul>
		</div>
	</body>
</html>
